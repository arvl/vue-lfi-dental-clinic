# 🦷 Clinic Management System for LFI Dental Clinic

Developed in compliance to the requirements of SE101, IPT101, HCI101, and IM101 in Quezon City University.

- Live site: [https://lfidentalclinic.vercel.app](https://lfidentalclinic.vercel.app)
- GitHub: [https://github.com/arvl130/nuxt-lfi-dental-clinic](https://github.com/arvl130/nuxt-lfi-dental-clinic)

Note: The repository for this project has been moved to [GitHub](https://github.com/arvl130/nuxt-lfi-dental-clinic). Please head over there for a more recent version.
